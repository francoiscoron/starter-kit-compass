# STARTER KIT COMPASS
Kit de démarage d'un projet compass. Les fichier styles sont organisé à la manière SMACSS. Les dossiers créer sont personnel, libre à vous de les modifier.

## Architecture du starter kit

```
starter-kit-compass/
|—— css
    |—— app.css
    |—— app.scss
    |—— base/
        |—— _base.scss
    |—— tool/
        |—— _settings.scss
    |—— Layout/
        |—— _header.scss
        |—— _nav.scss
        |—— _contain.scss
|—— img/
|—— js/
    |—— main.js
    |—— jquery.min.js
|—— config.rb
|—— index.html

```